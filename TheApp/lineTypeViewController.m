//
//  lineTypeViewController.m
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "lineTypeViewController.h"
#import "storage.h"

@implementation lineTypeViewController
@synthesize doneButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // load our storageClass singleton so we can access the line dash and thickness
    //
    StorageClass * obj= [StorageClass sharedStateInstance];  
    // Set the slider and switch to the current values of the line dash and thickness.
    //
    ltSlider.value = obj.lineThickness;
    dashSwitch.on = obj.lineType;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction) chooseLineThickness
{
    StorageClass * obj= [StorageClass sharedStateInstance];  
    obj.lineThickness = ltSlider.value;
    [self.view setNeedsDisplay]; 
}

- (IBAction) chooseDashedLine
{
    StorageClass * obj= [StorageClass sharedStateInstance]; 
    if (obj.lineType == 1)
    {
        obj.lineType = 0;
    }
    else 
        obj.lineType = 1;
    
    [self.view setNeedsDisplay]; 
}


- (IBAction) doneChoosingLine
{
    // close the view
    //
    [self dismissModalViewControllerAnimated:YES];
}

@end
