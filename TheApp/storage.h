//
//  storage.h
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// IT'S A SINGLETON! WOW :O
//
@interface StorageClass : NSObject
{
    NSMutableArray * globalShapeArray; 
    float lineThickness;
    int lineType;
    NSString * currentFileName;
}
@property(nonatomic,retain)NSMutableArray *globalShapeArray;   
@property(nonatomic,retain)NSString * currentFileName;   
@property float lineThickness;
@property int lineType;

+(StorageClass *) sharedStateInstance;    

@end
