//
//  canvas.h
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 2/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "shapeobj.h"
#import "ColorPickerViewController.h"

@interface canvas : UIViewController <ColorPickerViewControllerDelegate> 
{
    IBOutlet UIToolbar *topBar;
    IBOutlet UIView * canvasSurface;    
    IBOutlet UILabel * fileName;
    
    IBOutlet UIButton * toolButton;
    IBOutlet UIButton * colourButton;
    IBOutlet UIView * toolBox;
    IBOutlet NSLayoutConstraint * bottomBarSpaceFromBottom;
    shapeobj * currentShapeObj;
    UIColor * shapeColor;
    UIColor * shapeEdgeColor;
    bool takingPicture;
}

@property(readwrite,nonatomic,retain) IBOutlet UIToolbar *topBar;

@property(readwrite, nonatomic, retain) IBOutlet UIButton * toolButton;
@property(readwrite, nonatomic, retain) IBOutlet UIButton * colorButton;

@property(readwrite,nonatomic,retain) IBOutlet UILabel *fileName;


@property(readwrite, nonatomic, retain) IBOutlet UIView * toolBox;

@property(nonatomic, retain) IBOutlet NSLayoutConstraint * bottomBarSpaceFromBottom;

- (void) resetButtons;
- (IBAction) pressedNewFile;
- (IBAction) pressedSaveFile;
- (IBAction) pressedOpenFile;
- (IBAction) takePicture;
- (IBAction) colorButtonClicked;
- (IBAction) toolButtonClicked;
- (IBAction) choseLine;
- (IBAction) choseCircle;
- (IBAction) choseSquare;
- (IBAction) choseSelect;
- (IBAction) selectLineSize:(id)sender;
- (IBAction) selectColor:(id)sender;

@end

