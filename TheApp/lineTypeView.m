//
//  lineTypeView.m
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 3/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "lineTypeView.h"
#import "storage.h"


@implementation lineTypeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    //
    // Set the context for drawing:
    //
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Now load the singleton obj for the shape array.
    //
    StorageClass * obj= [StorageClass sharedStateInstance];  
    
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
    
    
    // set whether or not to draw a dashed outline
    //
    if (obj.lineType == 1)
    {
        CGFloat dashArray[] = {1};
        CGContextSetLineDash(context, 0,dashArray,1);
    }
    else
        CGContextSetLineDash(context, 0,0,0);
    
    
    // set the thickness of the line
    //
    CGContextSetLineWidth(context, obj.lineThickness);
    
    
    CGContextMoveToPoint(context, 50, 150);
    CGContextAddLineToPoint(context, 270, 150);
    CGContextStrokePath(context);
    
}


@end
