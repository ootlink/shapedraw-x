//
//  canvas.m
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 2/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

// canvas.h is really a canvas VIEW CONTROLLER. Now you know :) The more you know!
//
// 
#import "canvas.h"
// shape objects can be squares or circles or lines! oh my! 
//
#import "shapeobj.h"
// color picker.. self explanatory right?
//
#import "ColorPickerView.h"
// colorPicker stuff lets you pick colors! Pretty!
//
#import "ColorPickerViewController.h"
// canvasView is where the drawing is.. drawn
//
#import "canvasView.h"
// linetypecontroller lets you choose size and dashed-ness of the line
//
#import "lineTypeViewController.h"
// storage singleton! Handles the globalShapeArray, what to draw, and where to save things
//
#import "storage.h"
// This is supposed to handle file open (the tableview)
//
#import "openFileVC.h"

// allow saving as an image!
//
#import <QuartzCore/QuartzCore.h>


@implementation canvas


// synthesize the stuff that shows in the toolbar at the top and bottom.
//
@synthesize fileName;
@synthesize topBar;

// This is called when switching between modes in the toolbar.
//
- (void) resetButtons
{
}

- (IBAction) takePicture
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Exported!" message:@"saved to camera roll" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
    takingPicture = true;
}

- (IBAction) pressedNewFile
{
    // This clears out the current array, but leaves the tools
    // in their settings.
    //
    StorageClass * obj= [StorageClass sharedStateInstance];  
    [obj.globalShapeArray removeAllObjects];

    obj.currentFileName = @"Untitled";
    self.fileName.text = obj.currentFileName;

    canvasView * cSurface = (canvasView*)self.view;
    [cSurface reinit];
}

// Handle what happens if you press Save
// (A dialog box comes up asking you to put in a name)
//
- (IBAction) pressedSaveFile
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Save file as" message:@"If a file by this name already exists it will be overwritten!" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"save", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

// The actual dialog box is handled here.
// So is the saving file stuff.
//
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{ 
    StorageClass * obj= [StorageClass sharedStateInstance];  
    
    // this isn't needed but I kept it since I am out of time! 
    if (takingPicture == false)
    {
        if (buttonIndex == 1)
        {
            obj.currentFileName = [[alertView textFieldAtIndex:0] text];
            NSLog(@"Entered: %@",[[alertView textFieldAtIndex:0] text]);
            self.fileName.text = obj.currentFileName;
        }
        
        // Grab the file path of where to save the file.
        //
        
        NSError *error;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *plistDirectory = [paths objectAtIndex:0];
        NSString *fullPath = [plistDirectory stringByAppendingPathComponent:obj.currentFileName];

        
        // Now export the array over to a data plist
        // 
        NSData * drawingData = [NSKeyedArchiver archivedDataWithRootObject:obj.globalShapeArray];
        if (!drawingData)
        {
            NSLog(@"error converting data: %@", error);

        }
        
        // Save the actual data out. Let the user know if something went wrong.
        //
        if ([drawingData writeToFile:fullPath atomically:YES])
            NSLog(@"Successfully saved data");
        
        else
            NSLog(@"Couldn't write savefile!");
            
    }
    else
    {
        // We're taking a picture! YAY!
        //
        // Hide the toolbars :)
        //
        topBar.hidden = true;
        [self.view setNeedsDisplay];

        UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        NSData * pngPicFromView = UIImagePNGRepresentation(UIGraphicsGetImageFromCurrentImageContext()); // get PNG representation
        
        UIImage * picFromView = [UIImage imageWithData:pngPicFromView];
        
        UIGraphicsEndImageContext();
        
        UIImageWriteToSavedPhotosAlbum(picFromView,nil,nil,nil);

        
        // Show the toolbars again!
        //
        topBar.hidden = false;
        [self.view setNeedsDisplay];
        takingPicture = false;
    }
}

// The user can only save a file when there is a valid name. 
// It has to be at least ONE character hehe.
//
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if( [inputText length] >= 1 )
            return YES;
    return NO;
}

// What happens if a user pushes the open button!
// NOPE! Chuck Testa.  
//
- (IBAction) pressedOpenFile
{
    openFileVC * ofVC = 
    [[openFileVC alloc] initWithNibName:@"openFileVC" bundle:nil];
    [self presentModalViewController:ofVC animated:YES];
    
}

// If you hit the toolBox button, this function fires.
// like a rail gun.

- (IBAction) toolButtonClicked
{
    // Awww yiss!!
    if (self.bottomBarSpaceFromBottom.constant == 0)
        self.bottomBarSpaceFromBottom.constant = 140;
    else
        self.bottomBarSpaceFromBottom.constant = 0;
    
    [canvasSurface setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.25f animations:^{
        [canvasSurface layoutIfNeeded];
    }];

}

// We get to pick colors! I like colors. 
- (IBAction) colorButtonClicked
{
    // Here we go again
}


// What happens if a user decides they want to draw lines.
// 
- (IBAction) choseLine
{
    // self resetButtons turns all of the other buttons white and makes this one
    // a pretty shade of yellow. 
    //
    [self resetButtons];
    // this changes the color.. like I said, to yellow.
    // 
    
    // Here I'm using the variable surfaceType from the canvas View. It's the easy way.
    // Sorta sloppy but hey. 
    //
    canvasView * cSurface = (canvasView*)self.view;
    cSurface.shapeType = 0;
}

// Or if they want to draw circles
// 
- (IBAction) choseCircle
{
    // same as before, only with a circle.
    //
    [self resetButtons];
    canvasView * cSurface = (canvasView*)self.view;
    cSurface.shapeType = 1;
}
- (IBAction) choseSquare
{
    // same as before, but with rectangle shapes. 
    // 
    [self resetButtons];
    canvasView * cSurface = (canvasView*)self.view;
    cSurface.shapeType = 2;
}

- (IBAction) choseSelect
{
    // This feature is interesting and only half implemented. It lets you select
    // objects! Or.. it should. Instead it doesn't do ANYTHING.
    // 
    [self resetButtons];
    // anything they do from this point on (until they choose another button)
    // will only select existing objects! 
    canvasView * cSurface = (canvasView*)self.view;
    cSurface.shapeType = 3;
}

-(IBAction) selectColor:(id)sender {
    // This feature pops open the color picker. Yes, it uses the app delegate and has
    // a crappy NSUserDefaults feature I don't use. Yes, it was jacked from an example
    // of open source code that anyone can use. I admit it :) 
    // Most of the ideas in this project were plucked from stuff i found online :) 
    // 
    ColorPickerViewController *colorPickerViewController = 
    [[ColorPickerViewController alloc] initWithNibName:@"ColorPickerViewController" bundle:nil];
    colorPickerViewController.delegate = self;

    // You'll notice that I changed my ways around in the line size thing!
    //
    [self presentModalViewController:colorPickerViewController animated:YES];
}



-(IBAction) selectLineSize:(id)sender 
{
    // Here I just plain do fun stuff. :) 
    lineTypeViewController * lineTypeVC = 
    [[lineTypeViewController alloc] initWithNibName:@"lineTypeView" bundle:nil];
    
    [self presentModalViewController:lineTypeVC animated:YES];
}

- (void)colorPickerViewController:(ColorPickerViewController *)colorPicker didSelectColor:(UIColor *)color 
{    
    // No storage & check, just assign back the color
    canvasView * cSurface = (canvasView*)self.view;
    cSurface.shapeColor = color;
    [colorPicker dismissModalViewControllerAnimated:YES];
}

- (void)colorPickerViewController:(ColorPickerViewController *)colorPicker didSelectEdgeColor:(UIColor *)color 
{    
    // No storage & check, just assign back the color
    canvasView * cSurface = (canvasView*)self.view;
    cSurface.shapeEdgeColor = color;
    [colorPicker dismissModalViewControllerAnimated:YES];
}

// Nothing amazing here. drawLine is the default mode, so yea.
//
- (void)viewDidLoad {

    [super viewDidLoad];
}

// Load the toolbar, and everything else too! YAY!
//
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Tell navigationController that we want to show it's toolbar
    [self.navigationController setToolbarHidden:NO];
    
    
}







/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */
//- (void)drawRect:(CGRect)rect
//{
 //   
//}
- (void)viewDidUnload {
    canvasSurface = nil;
    [super viewDidUnload];
}
@end
