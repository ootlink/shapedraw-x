//
//  storage.m
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "storage.h"

@implementation StorageClass

@synthesize globalShapeArray;
@synthesize lineType;
@synthesize lineThickness;
@synthesize currentFileName;
static StorageClass *instance =nil;    

+(StorageClass *) sharedStateInstance
{
	static StorageClass * sharedStateInstance;
	
	@synchronized(self) {
		if(!sharedStateInstance) {
			sharedStateInstance = [[StorageClass alloc] init];
            // initialize the globalShapeArray. That's where all of the shape objects
            // are stored!
            //
			sharedStateInstance.globalShapeArray = [[NSMutableArray alloc] init];
            //
            // initialize the linetype stuff.
            //
            sharedStateInstance.lineType = 0;        // line type = solid by default
            sharedStateInstance.lineThickness = 1.0; // Line thickness = 1 by default
            
            sharedStateInstance.currentFileName = @"Untitled";
		}
	}
	return sharedStateInstance;
}

@end
