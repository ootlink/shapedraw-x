//
//  canvasView.h
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 2/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "shapeobj.h"

@interface canvasView : UIView
{
    // The stuff that holds the latest values!
    //
    CGPoint touch1;
	CGPoint touch2;
    UIColor * shapeColor;
    UIColor * shapeEdgeColor;
    int shapeType;
}

- (void) reinit;

@property int shapeType;
@property (nonatomic, retain) UIColor * shapeColor;
@property (nonatomic, retain) UIColor * shapeEdgeColor;
@end
