//
//  lineTypeViewController.h
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class lineTypeViewController;



@interface lineTypeViewController : UIViewController
{
    IBOutlet UIButton *doneButton;
    IBOutlet UISlider *ltSlider;
    IBOutlet UISwitch *dashSwitch;
}

@property(readwrite,nonatomic,retain) IBOutlet UIButton *doneButton;


- (IBAction) doneChoosingLine;
- (IBAction) chooseLineThickness;
- (IBAction) chooseDashedLine;

@end
