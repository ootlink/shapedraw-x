//
//  canvasView.m
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 2/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "canvasView.h"
#import "canvas.h"
#import "storage.h"

@implementation canvasView

@synthesize shapeColor;
@synthesize shapeEdgeColor;
@synthesize shapeType;


- (void) reinit
{
    touch1 = touch2;
    [self setNeedsDisplay];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //
//        NSMutableArray * localShapeArray = [[NSMutableArray alloc] initWithCapacity:1];
        
    }
    return self;
}

// Touching is creepy. 

- (void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event { 
    
    // Here we setup what happens when you touch!
    // 
    UITouch *touch = [touches anyObject];
    touch1 = [touch locationInView:self]; 
    touch2 = [touch locationInView:self]; 
    
    [self setNeedsDisplay]; 
    
} 

// Here is where one is dragging their finger across the screen to draw stuff.
//
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    touch2 = [touch locationInView:self]; 
    
    [self setNeedsDisplay]; 
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    // Load the singleton obj
    //
    StorageClass * obj= [StorageClass sharedStateInstance];  

    
    // Do we make an object, or are we selecting an object that already exists? 
    // 

    if (shapeType == 3)
    {
        // There are two things about this. The first one is that you can only
        // select one object at a time. The second one is, that it only
        // accepts tap input. 
        // 
        
        if ((touch1.x == touch2.x) && (touch1.y == touch2.y))
            
        {
            // Now we search the globalShapeArray for objects that fit within this.
            // FROM THE BOTTOM. The most recent object (if there are any overlapping)
            // will be the one that gets selected. 
            //
        }

        // We're done here. 
        //
        return;
    }    
    // Make a shape object to dump into the array. 
    //
    shapeobj * currentShapeObj = [[shapeobj alloc] initWithShape:shapeType andColor:shapeColor andEdgeColor:shapeEdgeColor andLineType:obj.lineType andLineThickness:obj.lineThickness andTL:touch1 andBR:touch2];
    
    // Now dump it in the array
    // 
    [obj.globalShapeArray addObject:currentShapeObj];
    
    // DEBUG: and finally, tell the user how many there are.
    //
    NSLog (@"Objects in Array: %i", [obj.globalShapeArray count]);
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//
//

// The reason why there's no if (shapeType=3) don't draw this object thing, is
// I wanted to allow people to drag a selection square just like you can in photoshop
// or maybe even MS Paint? XD
//
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    //
    // Set the context for drawing:
    //
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Now load the singleton obj for the shape array.
    //
    StorageClass * obj= [StorageClass sharedStateInstance];  

    
    // Draw all of the shape objects in the database. 
    //
    
    shapeobj * iObject; 
    
    for (int i= [obj.globalShapeArray count], j= i; i > 0; i--) 
    {
        iObject = [obj.globalShapeArray objectAtIndex:(j-i)];
        // draw each element
        //
        
        // set the fill and edge colors
        //
        CGContextSetStrokeColorWithColor(context, iObject.shapeEdgeColor.CGColor);
        CGContextSetFillColorWithColor(context, iObject.shapeColor.CGColor);
        
        // set whether or not to draw a dashed outline
        //
        if (iObject.lineType == 1)
        {
            CGFloat dashArray[] = {1};
            CGContextSetLineDash(context, 0,dashArray,1);
        }
        else
            // If the lineType was previously toggled it'll stay there.
            // so we get rid of the dash if it's not wanted.
            //
            CGContextSetLineDash(context, 0,0,0);
        
        // set the thickness of the line
        //
        CGContextSetLineWidth(context, iObject.lineThickness);
        
        // then draw it!
        //
        CGRect currentRect = CGRectMake (
                                         (iObject.topLeft.x > iObject.bottomRight.x) ? iObject.bottomRight.x : iObject.topLeft.x,
                                         (iObject.topLeft.y > iObject.bottomRight.y) ? iObject.bottomRight.y : iObject.topLeft.y,
                                         fabsf(iObject.topLeft.x - iObject.bottomRight.x),
                                         fabsf(iObject.topLeft.y - iObject.bottomRight.y));
        
        
        switch (iObject.shapeType) {
                // line
            case 0:
                CGContextMoveToPoint(context, iObject.topLeft.x, iObject.topLeft.y);
                CGContextAddLineToPoint(context, iObject.bottomRight.x, iObject.bottomRight.y);
                CGContextStrokePath(context);
                break;
                // circle
            case 1:
                CGContextAddEllipseInRect(context, currentRect);
                CGContextDrawPath(context, kCGPathFillStroke);
                break;
                // square
            case 2:
                CGContextAddRect(context, currentRect);
                CGContextDrawPath(context, kCGPathFillStroke);
                break;
            default:
                break;
        }
        
        
        // move on to the next element.
        //
    }
 
    // Then draw the current one - using the current properties.. 
    //

    CGContextSetStrokeColorWithColor(context, shapeEdgeColor.CGColor);
    CGContextSetFillColorWithColor(context, shapeColor.CGColor);
    
    
    // set whether or not to draw a dashed outline
    //
    if (obj.lineType == 1)
    {
        CGFloat dashArray[] = {1};
        CGContextSetLineDash(context, 0,dashArray,1);
    }
    else
        CGContextSetLineDash(context, 0,0,0);
    
    
    // set the thickness of the line
    //
    CGContextSetLineWidth(context, obj.lineThickness);
    
    // then draw it!
    //
    CGRect currentRect = CGRectMake (
                                     (touch1.x > touch2.x) ? touch2.x : touch1.x,
                                     (touch1.y > touch2.y) ? touch2.y : touch1.y,
                                     fabsf(touch1.x - touch2.x),
                                     fabsf(touch1.y - touch2.y));


    switch (shapeType) {
        // line
        case 0:
            CGContextMoveToPoint(context, touch1.x, touch1.y);
            CGContextAddLineToPoint(context, touch2.x, touch2.y);
            CGContextStrokePath(context);
            break;
        // circle
        case 1:
            CGContextAddEllipseInRect(context, currentRect);
            CGContextDrawPath(context, kCGPathFillStroke);
            break;
        // square
        case 2:
            CGContextAddRect(context, currentRect);
            CGContextDrawPath(context, kCGPathFillStroke);
            break;
        default:
            break;
    }


}


@end
