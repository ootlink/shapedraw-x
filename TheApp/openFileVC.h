//
//  openFileVC.h
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface openFileVC : UIViewController
{
    // Not much love going on here :\
    //
    IBOutlet UIButton *cancelButton;
    UITableView * fileTable;
}

@property(readwrite,nonatomic,retain) IBOutlet UIButton *cancelButton;
@property(readwrite,nonatomic,retain) IBOutlet UITableView * fileTable;


- (IBAction) cancelOpen;

@end
