//
//  shapeobj.h
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// I got lazy, not much commenting here. sorry. 
//
@interface shapeobj : NSObject <NSCoding>
{
	CGPoint topLeft;
	CGPoint bottomRight;
    UIColor * shapeColor;
    UIColor * shapeEdgeColor;
    
    int shapeType;
    int lineType;
    float lineThickness;
}

@property CGPoint topLeft;
@property CGPoint bottomRight;
@property int shapeType;
@property (nonatomic, retain) UIColor * shapeColor;
@property (nonatomic, retain) UIColor * shapeEdgeColor;
@property int lineType;
@property float lineThickness;


-(id)initWithShape: (int)typeOfShape andColor:(UIColor *)colorOfShape andEdgeColor:(UIColor *)edgeColor andLineType:(int)typeOfLine andLineThickness:(float)lthickness andTL:(CGPoint)topLeftPos andBR:(CGPoint)bottomLeftPos;

@end
