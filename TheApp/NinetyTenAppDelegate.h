//
//  NinetyTenAppDelegate.h
//  TheApp
//
//  Created by Joseph Cranmer on 4/1/14.
//  Copyright (c) 2014 JJ Cranmer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NinetyTenAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
