//
//  main.m
//  TheApp
//
//  Created by Joseph Cranmer on 4/1/14.
//  Copyright (c) 2014 JJ Cranmer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NinetyTenAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NinetyTenAppDelegate class]));
    }
}
