//
//  shapeobj.m
//  ShapeDraw X
//
//  Created by Joseph Cranmer on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "shapeobj.h"

@implementation shapeobj
@synthesize topLeft;
@synthesize bottomRight;
@synthesize shapeColor;
@synthesize shapeEdgeColor;
@synthesize shapeType;
@synthesize lineType;
@synthesize lineThickness;



-(id)initWithShape: (int)typeOfShape andColor:(UIColor *)colorOfShape andEdgeColor:(UIColor *)edgeColor andLineType:(int)typeOfLine andLineThickness:(float)lthickness andTL:(CGPoint)topLeftPos andBR:(CGPoint)bottomRightPos
{
    topLeft = topLeftPos;
    bottomRight = bottomRightPos;
    shapeColor = colorOfShape;
    shapeEdgeColor = edgeColor;
    shapeType = typeOfShape;
    lineType = typeOfLine;
    lineThickness = lthickness;
        
    return self;
}


// Since shapeonj uses some funny stuff, there are encoders and decoders to handle save/open file. Yep. NSCoding. 
// SWEET!
// 
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeCGPoint:topLeft forKey:@"top left position"];
    [encoder encodeCGPoint:bottomRight forKey:@"bottom right position"];
    
    [encoder encodeObject:shapeColor forKey:@"shape color"];
    [encoder encodeObject:shapeEdgeColor forKey:@"shape edge color"];
    
    [encoder encodeInt:shapeType forKey:@"shape type"];
    [encoder encodeInt:lineType forKey:@"line type"];
    [encoder encodeFloat:lineThickness forKey:@"line thickness"];

    
}

- (id)initWithCoder:(NSCoder *)decoder {

    topLeft = [decoder decodeCGPointForKey:@"top left position"];
    bottomRight = [decoder decodeCGPointForKey:@"bottom right position"];
    bottomRight = [decoder decodeCGPointForKey:@"bottom right position"];
    shapeColor = [decoder decodeObjectForKey:@"bottom right position"];
    shapeEdgeColor = [decoder decodeObjectForKey:@"bottom right position"];
    shapeType = [decoder decodeIntForKey:@"bottom right position"];
    lineType = [decoder decodeIntForKey:@"bottom right position"];
    lineThickness = [decoder decodeFloatForKey:@"bottom right position"];

    return [self initWithShape:shapeType andColor:shapeColor andEdgeColor:shapeEdgeColor andLineType:lineType andLineThickness:lineThickness andTL:topLeft andBR:bottomRight];
}


@end
